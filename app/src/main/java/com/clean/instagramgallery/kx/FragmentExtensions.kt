package com.clean.instagramgallery.kx

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

inline fun <reified T : ViewModel> Fragment.viewModel(
    factory: ViewModelProvider.Factory,
    parentActivity: FragmentActivity? = null
): T {
    return if (parentActivity != null)
        ViewModelProviders.of(parentActivity, factory)[T::class.java]
    else
        return ViewModelProviders.of(this, factory)[T::class.java]
}