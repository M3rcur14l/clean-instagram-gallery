package com.clean.instagramgallery.di.module

import android.content.Context
import androidx.room.Room
import com.clean.instagramgallery.data.datasource.InstagramRemoteDataSource
import com.clean.instagramgallery.data.datasource.LocalDataSource
import com.clean.instagramgallery.data.manager.InstagramAccountManager
import com.clean.instagramgallery.data.persistence.AppDatabase
import com.clean.instagramgallery.data.repository.RepositoryImpl
import com.clean.instagramgallery.data.service.InstagramService
import com.clean.instagramgallery.domain.repository.Repository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class DataModule {

    @Singleton
    @Provides
    internal fun providesInstagramService(retrofit: Retrofit) = retrofit.create(InstagramService::class.java)

    @Singleton
    @Provides
    internal fun providesDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "instagram-gallery-database").build()
    }

    @Singleton
    @Provides
    internal fun providesDatabaseDao(database: AppDatabase) = database.feedDao()

    @Singleton
    @Provides
    internal fun providesRepository(
        instagramAccountManager: InstagramAccountManager,
        gitHubRemoteDataSource: InstagramRemoteDataSource,
        localDataSource: LocalDataSource
    ): Repository = RepositoryImpl(
        instagramAccountManager,
        gitHubRemoteDataSource,
        localDataSource
    )

}