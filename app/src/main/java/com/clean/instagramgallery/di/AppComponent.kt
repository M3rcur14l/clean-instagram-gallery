package com.clean.instagramgallery.di

import com.clean.instagramgallery.InstagramGalleryApplication
import com.clean.instagramgallery.di.module.*
import com.clean.instagramgallery.di.module.fragment.FragmentModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityModule::class,
        ApplicationModule::class,
        FragmentModule::class,
        ViewModelModule::class,
        NetworkModule::class,
        DataModule::class,
        AccountModule::class]
)
interface AppComponent : AndroidInjector<InstagramGalleryApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: InstagramGalleryApplication): Builder

        fun build(): AppComponent
    }

    override fun inject(application: InstagramGalleryApplication)

}
