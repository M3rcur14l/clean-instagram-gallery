package com.clean.instagramgallery.di.module

import android.content.Context
import com.clean.instagramgallery.InstagramGalleryApplication
import dagger.Binds
import dagger.Module

@Module
abstract class ApplicationModule {

    @Binds
    abstract fun bindContext(application: InstagramGalleryApplication): Context

}