package com.clean.instagramgallery.di.module

import com.clean.instagramgallery.InstagramGalleryApplication
import com.clean.instagramgallery.data.manager.InstagramAccountManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AccountModule {

    @Singleton
    @Provides
    fun providesInstagramAccountManager(application: InstagramGalleryApplication) =
        InstagramAccountManager(application)

}