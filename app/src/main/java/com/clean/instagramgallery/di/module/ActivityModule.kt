package com.clean.instagramgallery.di.module

import com.clean.instagramgallery.presentation.main.MainActivity
import com.clean.instagramgallery.di.module.fragment.FragmentModule
import com.clean.instagramgallery.di.scope.ActivityScoped
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun mainActivity(): MainActivity

}
