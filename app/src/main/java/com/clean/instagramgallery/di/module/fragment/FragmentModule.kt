package com.clean.instagramgallery.di.module.fragment

import com.clean.instagramgallery.di.scope.FragmentScoped
import com.clean.instagramgallery.presentation.detail.CarouselFragment
import com.clean.instagramgallery.presentation.login.LoginFragment
import com.clean.instagramgallery.presentation.main.GridFragment
import com.clean.instagramgallery.presentation.main.MainFragment
import com.clean.instagramgallery.presentation.main.StaggeredFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun loginFragment(): LoginFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun mainFragment(): MainFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun staggeredFragment(): StaggeredFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun gridFragment(): GridFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun carouselFragment(): CarouselFragment

}