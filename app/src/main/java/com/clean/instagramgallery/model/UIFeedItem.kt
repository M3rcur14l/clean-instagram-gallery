package com.clean.instagramgallery.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UIFeedItem(
    val id: String,
    val date: String,
    val thumbnail: String,
    val image: String,
    val aspectRatio: String,
    val caption: String,
    val likes: Int,
    val comments: Int
) : Parcelable