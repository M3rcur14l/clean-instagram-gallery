package com.clean.instagramgallery.presentation.main

import androidx.lifecycle.MutableLiveData
import com.clean.instagramgallery.R
import com.clean.instagramgallery.data.bus.Bus
import com.clean.instagramgallery.data.bus.annotation.ExecutionState.Companion.FAILED
import com.clean.instagramgallery.data.bus.state.LoadingState
import com.clean.instagramgallery.domain.repository.Repository
import com.clean.instagramgallery.kx.bindToLifecycle
import com.clean.instagramgallery.mapper.asUIModel
import com.clean.instagramgallery.model.UIFeedItem
import com.clean.instagramgallery.presentation.LifecycleViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val repository: Repository
) : LifecycleViewModel() {

    init {
        repository.bindTo(this)
    }

    val feedList = MutableLiveData<List<UIFeedItem>>()

    fun getFeeds() {
        repository.getFeed()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { list ->
                    feedList.value = list.map { it.asUIModel() }
                },
                {
                    Bus.postState(LoadingState(FAILED, R.string.failure_local_feed_load))
                })
            .bindToLifecycle(this)
    }

}
