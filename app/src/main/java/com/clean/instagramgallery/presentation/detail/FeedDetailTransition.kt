package com.clean.instagramgallery.presentation.detail

import androidx.transition.ChangeImageTransform
import androidx.transition.Explode
import androidx.transition.TransitionSet

class FeedDetailTransition : TransitionSet() {

    init {
        ordering = ORDERING_TOGETHER
        addTransition(Explode())
            .addTransition(ChangeImageTransform())
    }
}