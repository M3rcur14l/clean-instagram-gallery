package com.clean.instagramgallery.presentation.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.clean.instagramgallery.R
import com.clean.instagramgallery.kx.viewModel
import com.clean.instagramgallery.model.UIFeedItem
import com.clean.instagramgallery.presentation.main.FeedsAdapter
import com.clean.instagramgallery.presentation.main.MainViewModel
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.carousel_fragment.*
import javax.inject.Inject

class CarouselFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var mainViewModel: MainViewModel
    lateinit var adapter: FeedsAdapter

    companion object {
        private const val FEED_EXTRA = "FEED_EXTRA"

        fun newInstance(feed: UIFeedItem): CarouselFragment {
            val fragment = CarouselFragment()
            val extras = Bundle()
            extras.putParcelable(FEED_EXTRA, feed)
            fragment.arguments = extras
            fragment.postponeEnterTransition()
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.carousel_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        arguments?.let { args ->
            val feed = args.getParcelable<UIFeedItem>(FEED_EXTRA)
            adapter = FeedsAdapter(FeedsAdapter.Visualization.DETAIL)
            carousel.adapter = adapter
            val snapHelper = PagerSnapHelper()
            snapHelper.attachToRecyclerView(carousel)
            carousel.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            mainViewModel = viewModel(viewModelFactory, activity)
            mainViewModel.feedList.observe(this) {
                adapter.submitList(it)
            }
            mainViewModel = viewModel(viewModelFactory, activity)
            mainViewModel.feedList.observe(this) {
                carousel.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
                    override fun onPreDraw(): Boolean {
                        carousel.viewTreeObserver.removeOnPreDrawListener(this)
                        startPostponedEnterTransition()
                        return true
                    }
                })
                adapter.submitList(it)
                feed?.let { feed -> carousel.scrollToPosition(it.indexOf(feed)) }
            }

        }

    }

}
