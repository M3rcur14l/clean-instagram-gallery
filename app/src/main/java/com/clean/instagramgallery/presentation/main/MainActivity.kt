package com.clean.instagramgallery.presentation.main

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import com.clean.instagramgallery.R
import com.clean.instagramgallery.data.BuildConfig
import com.clean.instagramgallery.data.manager.InstagramAccountManager
import com.clean.instagramgallery.presentation.BaseActivity
import com.clean.instagramgallery.presentation.login.LoginFragment
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.LENGTH_LONG
import kotlinx.android.synthetic.main.main_activity.*
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var accountManager: InstagramAccountManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(
                    R.id.container,
                    if (accountManager.accessToken != null || !BuildConfig.ACCESS_TOKEN.isEmpty())
                        MainFragment.newInstance()
                    else
                        LoginFragment.newInstance()
                )
                .commitNow()
        }
    }

    override fun onStart() {
        super.onStart()
        if (!isNetworkAvailable())
            Snackbar.make(main, R.string.offline_message, LENGTH_LONG).show()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1)
            supportFragmentManager.popBackStack()
        else
            super.onBackPressed()
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}
