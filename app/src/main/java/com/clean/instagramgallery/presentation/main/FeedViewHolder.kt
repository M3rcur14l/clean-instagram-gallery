package com.clean.instagramgallery.presentation.main

import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.clean.instagramgallery.model.UIFeedItem
import com.clean.instagramgallery.presentation.main.FeedsAdapter.Visualization.DETAIL
import com.clean.instagramgallery.presentation.main.FeedsAdapter.Visualization.GRID
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item.*

class FeedViewHolder(
    override val containerView: ConstraintLayout,
    private val onClick: (Pair<UIFeedItem, View>) -> (Unit)
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(model: UIFeedItem, visualization: FeedsAdapter.Visualization) {
        val set = ConstraintSet()
        set.clone(containerView)
        set.setDimensionRatio(
            feedImage.id,
            if (visualization == GRID) "1:1"
            else model.aspectRatio
        )
        set.applyTo(containerView)

        if (visualization != DETAIL) {
            likeNumber.text = model.likes.toString()
            commentNumber.text = model.comments.toString()
        }

        Glide.with(itemView.context)
            .load(if (visualization == GRID) model.thumbnail else model.image)
            .into(feedImage)
        ViewCompat.setTransitionName(feedImage, "feed_${model.id}")
        if (visualization != DETAIL)
            feedback.setOnClickListener { onClick.invoke(Pair(model, feedImage)) }
    }
}