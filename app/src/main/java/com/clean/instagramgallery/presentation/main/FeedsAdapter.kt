package com.clean.instagramgallery.presentation.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.clean.instagramgallery.R
import com.clean.instagramgallery.model.UIFeedItem

class FeedsAdapter(private val visualization: Visualization) : ListAdapter<UIFeedItem, FeedViewHolder>(ItemCallBack()) {

    val feedClick = MutableLiveData<Pair<UIFeedItem, View>>()

    class ItemCallBack : DiffUtil.ItemCallback<UIFeedItem>() {
        override fun areItemsTheSame(oldItem: UIFeedItem, newItem: UIFeedItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: UIFeedItem, newItem: UIFeedItem): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            if (visualization == Visualization.DETAIL) R.layout.list_item_detail else R.layout.list_item,
            parent,
            false
        )
        return FeedViewHolder(view as ConstraintLayout) { feedClick.value = it }
    }

    override fun onBindViewHolder(holder: FeedViewHolder, position: Int) {
        holder.bind(getItem(position), visualization)
    }

    enum class Visualization {
        STAGGERED,
        GRID,
        DETAIL
    }
}