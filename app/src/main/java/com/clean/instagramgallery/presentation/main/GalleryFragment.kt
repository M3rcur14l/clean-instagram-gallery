package com.clean.instagramgallery.presentation.main

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.Fade
import com.clean.instagramgallery.R
import com.clean.instagramgallery.kx.viewModel
import com.clean.instagramgallery.presentation.detail.CarouselFragment
import com.clean.instagramgallery.presentation.detail.FeedDetailTransition
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.gallery_fragment.*
import javax.inject.Inject


abstract class GalleryFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var mainViewModel: MainViewModel
    lateinit var adapter: FeedsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.gallery_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = FeedsAdapter(visualization())
        feedsList.adapter = adapter
        feedsList.addItemDecoration(GridSpaceItemDecoration(resources.getDimensionPixelSize(R.dimen.grid_cell_spacing)))
        feedsList.layoutManager = layoutManager()
        adapter.feedClick.observe(this) {
            val carouselFragment = CarouselFragment.newInstance(it.first)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                carouselFragment.sharedElementEnterTransition = FeedDetailTransition()
                carouselFragment.sharedElementReturnTransition = FeedDetailTransition()
                carouselFragment.enterTransition = Fade()
                exitTransition = Fade()
            }
            activity?.supportFragmentManager?.beginTransaction()
                ?.addSharedElement(it.second, ViewCompat.getTransitionName(it.second) ?: "")
                ?.add(R.id.container, carouselFragment)
                ?.addToBackStack(null)
                ?.commit()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mainViewModel = viewModel(viewModelFactory, activity)
        mainViewModel.feedList.observe(this) {
            adapter.submitList(it)
        }
    }

    abstract fun layoutManager(): RecyclerView.LayoutManager

    abstract fun visualization(): FeedsAdapter.Visualization

}
