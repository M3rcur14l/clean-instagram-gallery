package com.clean.instagramgallery.presentation.login

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.CookieManager
import com.clean.instagramgallery.BuildConfig
import com.clean.instagramgallery.R
import com.clean.instagramgallery.data.manager.InstagramAccountManager
import com.clean.instagramgallery.presentation.main.MainFragment
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.login_fragment.*
import javax.inject.Inject

class LoginFragment : DaggerFragment() {

    @Inject
    lateinit var accountManager: InstagramAccountManager

    companion object {
        fun newInstance() = LoginFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginWebView.settings.apply {
            javaScriptEnabled = true
            CookieManager.getInstance().setAcceptCookie(true)
        }
        loginWebView.webViewClient = LoginWebViewClient {
            accountManager.accessToken = it
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.container, MainFragment.newInstance())
                ?.commitNow()
        }
        loginWebView.loadUrl(BuildConfig.INSTAGRAM_LOGIN_URL)
    }

}
