package com.clean.instagramgallery.presentation.main

import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager.VERTICAL


class StaggeredFragment : GalleryFragment() {

    companion object {
        fun newInstance() = StaggeredFragment()
    }

    override fun layoutManager() = StaggeredGridLayoutManager(2, VERTICAL)

    override fun visualization() = FeedsAdapter.Visualization.STAGGERED

}
