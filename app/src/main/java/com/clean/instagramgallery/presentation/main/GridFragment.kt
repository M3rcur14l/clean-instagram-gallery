package com.clean.instagramgallery.presentation.main

import androidx.recyclerview.widget.GridLayoutManager


class GridFragment : GalleryFragment() {

    companion object {
        fun newInstance() = GridFragment()
    }

    override fun layoutManager() = GridLayoutManager(context, 3)

    override fun visualization() = FeedsAdapter.Visualization.GRID

}
