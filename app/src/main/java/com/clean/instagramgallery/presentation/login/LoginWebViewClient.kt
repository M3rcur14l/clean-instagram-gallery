package com.clean.instagramgallery.presentation.login

import android.net.Uri
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import com.clean.instagramgallery.BuildConfig

class LoginWebViewClient(private val onTokenReceived: (String?) -> Unit) : WebViewClient() {

    override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
        Log.d("InstagramAPP", "WebView url received: $url")
        return if (url?.contains(BuildConfig.INSTAGRAM_LOGIN_AUTHORIZED_URL) == true) {
            val accessToken = Uri.parse(url).getQueryParameter("accessToken")
            onTokenReceived.invoke(accessToken)
            true
        } else
            super.shouldOverrideUrlLoading(view, url)
    }

}