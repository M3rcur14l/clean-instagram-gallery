package com.clean.instagramgallery.presentation.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.clean.instagramgallery.R
import com.clean.instagramgallery.data.bus.Bus
import com.clean.instagramgallery.data.bus.annotation.ExecutionState.Companion.FAILED
import com.clean.instagramgallery.data.bus.state.LoadingState
import com.clean.instagramgallery.kx.viewModel
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.main_fragment.*
import javax.inject.Inject


class MainFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var mainViewModel: MainViewModel
    private var staggeredFragment: StaggeredFragment? = null
    private var gridFragment: GridFragment? = null

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val staggeredFragment = StaggeredFragment.newInstance()
        val gridFragment = GridFragment.newInstance()
        fragmentManager?.let {
            val tabAdapter = TabAdapter(
                it,
                listOf(staggeredFragment, gridFragment),
                listOf(
                    activity?.resources?.getString(R.string.staggered) ?: "",
                    activity?.resources?.getString(R.string.grid) ?: ""
                )
            )
            pager.adapter = tabAdapter
            tabLayout.setupWithViewPager(pager)
        }
        this.staggeredFragment = staggeredFragment
        this.gridFragment = gridFragment
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Bus.subscribeToState(LoadingState::class.java) {
            if (it.state == FAILED)
                Snackbar.make(
                    mainFragment,
                    activity?.resources?.getString(it.stringResource ?: R.string.generic_error) ?: "",
                    Snackbar.LENGTH_LONG
                ).show()
        }
        mainViewModel = viewModel(viewModelFactory, activity)
        mainViewModel.getFeeds()
    }

}
