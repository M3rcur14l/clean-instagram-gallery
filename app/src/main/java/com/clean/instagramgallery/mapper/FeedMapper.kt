package com.clean.instagramgallery.mapper

import com.clean.instagramgallery.domain.model.Feed
import com.clean.instagramgallery.model.UIFeedItem
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.abs

fun Feed.asUIModel() = UIFeedItem(
    id = id,
    date = UI_DATE_FORMAT.format(date),
    thumbnail = thumbnail,
    image = image,
    aspectRatio = nearestAspectRatio(aspectRatio),
    caption = caption,
    likes = likes,
    comments = comments
)

val UI_DATE_FORMAT = SimpleDateFormat("yyyy/MM/dd 'at' HH:mm:ss", Locale.ITALY)

fun nearestAspectRatio(ar: Double): String {
    var w = 1
    var h = 1
    for (n in 1..19) {
        val m = (ar * n + 0.5)
        if (abs(ar - m / n) < 0.1) {
            w = m.toInt()
            h = n
        }
    }
    return "$w:$h"
}