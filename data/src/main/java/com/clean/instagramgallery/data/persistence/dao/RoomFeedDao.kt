package com.clean.instagramgallery.data.persistence.dao

import androidx.room.*
import com.clean.instagramgallery.data.model.local.RoomFeed
import io.reactivex.Flowable

@Dao
interface RoomFeedDao {

    @Query("SELECT * FROM feed ORDER BY date desc")
    fun getFeed(): Flowable<List<RoomFeed>>

    @Delete
    fun deleteFeed(feed: RoomFeed)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertFeedList(data: List<RoomFeed>?)
}