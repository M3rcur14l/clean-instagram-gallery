package com.clean.instagramgallery.data.repository

import com.clean.instagramgallery.data.BuildConfig
import com.clean.instagramgallery.data.R
import com.clean.instagramgallery.data.bus.Bus
import com.clean.instagramgallery.data.bus.annotation.ExecutionState.Companion.FAILED
import com.clean.instagramgallery.data.bus.state.LoadingState
import com.clean.instagramgallery.data.datasource.InstagramRemoteDataSource
import com.clean.instagramgallery.data.datasource.LocalDataSource
import com.clean.instagramgallery.data.kx.bindToLifecycle
import com.clean.instagramgallery.data.manager.InstagramAccountManager
import com.clean.instagramgallery.domain.LifecycleBinder
import com.clean.instagramgallery.domain.SimpleLifecycleBinder
import com.clean.instagramgallery.domain.model.Feed
import com.clean.instagramgallery.domain.repository.Repository
import io.reactivex.Flowable
import javax.inject.Inject

class RepositoryImpl @Inject constructor(
    private val accountManager: InstagramAccountManager,
    private val instagramRemoteDataSource: InstagramRemoteDataSource,
    private val localDataSource: LocalDataSource
) : Repository, LifecycleBinder by SimpleLifecycleBinder() {

    private val localAccessToken: String = BuildConfig.ACCESS_TOKEN

    override fun getFeed(): Flowable<List<Feed>> {
        instagramRemoteDataSource.getFeeds(accountManager.accessToken ?: localAccessToken)
            .subscribe({
                localDataSource.insertFeeds(it)
            }, {
                Bus.postState(LoadingState(FAILED, R.string.failure_remote_feed_load))
            }).bindToLifecycle(this)
        return localDataSource.getFeed()
    }

}