package com.clean.instagramgallery.data.service

import com.clean.instagramgallery.data.model.remote.InstagramRecentMedia
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface InstagramService {

    @GET("users/self/media/recent/")
    fun getRecentMedia(
        @Query("access_token")
        accessToken: String
    ): Single<InstagramRecentMedia>
}