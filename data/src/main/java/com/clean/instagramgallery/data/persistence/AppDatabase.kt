package com.clean.instagramgallery.data.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.clean.instagramgallery.data.model.local.RoomFeed
import com.clean.instagramgallery.data.persistence.dao.RoomFeedDao

@Database(
    entities = [RoomFeed::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun feedDao(): RoomFeedDao

}