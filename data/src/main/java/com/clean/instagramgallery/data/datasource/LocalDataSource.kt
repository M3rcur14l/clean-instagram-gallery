package com.clean.instagramgallery.data.datasource

import com.clean.instagramgallery.data.mapper.asDomainModel
import com.clean.instagramgallery.data.mapper.asLocalDataModel
import com.clean.instagramgallery.data.persistence.dao.RoomFeedDao
import com.clean.instagramgallery.domain.model.Feed
import io.reactivex.Flowable
import javax.inject.Inject

class LocalDataSource @Inject constructor(private val feedDao: RoomFeedDao) {

    fun getFeed(): Flowable<List<Feed>> {
        return feedDao
            .getFeed()
            .map { list -> list.map { it.asDomainModel() } }
    }

    fun insertFeeds(list: List<Feed>) {
        feedDao.insertFeedList(list.map { it.asLocalDataModel() })
    }
}