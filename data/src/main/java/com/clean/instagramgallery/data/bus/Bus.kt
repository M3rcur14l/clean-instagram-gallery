package com.clean.instagramgallery.data.bus

import com.clean.instagramgallery.data.bus.annotation.DeliveryThread
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit

/**
 * Created by  Antonello Fodde on 10/04/2017.
 * antonello.fodde@accenture.com
 */
object Bus {

    private val mEventBusSubjectMap = ConcurrentHashMap<Class<*>, Subject<Any>>()
    private val mStateBusSubjectMap = ConcurrentHashMap<Class<*>, Subject<Any>>()

    fun <T> subscribeToEvent(eventClass: Class<T>, onNext: (T) -> Unit): Disposable? {
        return subscribeToEvent(eventClass, onNext, DeliveryThread.MAIN_THREAD)
    }

    fun <T> subscribeToState(stateClass: Class<T>, onNext: (T) -> Unit): Disposable? {
        return subscribeToState(stateClass, onNext, DeliveryThread.MAIN_THREAD)
    }

    fun <T> subscribeToEvent(
        eventClass: Class<T>, onNext: (T) -> Unit,
        @DeliveryThread deliveryThread: Int
    ): Disposable? {
        eventSubjectCheck(eventClass)
        return mEventBusSubjectMap[eventClass]
            ?.cast(eventClass)
            ?.observeOn(getScheduler(deliveryThread))
            ?.subscribe(onNext)
    }

    fun <T> subscribeToState(
        stateClass: Class<T>, onNext: (T) -> Unit,
        @DeliveryThread deliveryThread: Int
    ): Disposable? {
        stateSubjectCheck(stateClass)
        return mStateBusSubjectMap[stateClass]
            ?.cast(stateClass)
            ?.observeOn(getScheduler(deliveryThread))
            ?.subscribe(onNext)
    }

    fun <T> hasSubscriberForEvent(eventClass: Class<T>): Boolean {
        eventSubjectCheck(eventClass)
        return mEventBusSubjectMap[eventClass]?.hasObservers() ?: false
    }

    fun <T> hasSubscriberForState(stateClass: Class<T>): Boolean {
        stateSubjectCheck(stateClass)
        return mStateBusSubjectMap[stateClass]?.hasObservers() ?: false
    }

    fun postEvent(event: Any) {
        eventSubjectCheck(event.javaClass)
        mEventBusSubjectMap[event.javaClass]?.onNext(event)
    }

    fun postDelayedEvent(event: Any, delay: Long, unit: TimeUnit): Disposable {
        return Observable.timer(delay, unit)
            .subscribe { postEvent(event) }
    }

    fun postState(state: Any) {
        stateSubjectCheck(state.javaClass)
        mStateBusSubjectMap[state.javaClass]?.onNext(state)
    }

    fun postDelayedState(state: Any, delay: Long, unit: TimeUnit): Disposable {
        return Observable.timer(delay, unit)
            .subscribe { postState(state) }
    }

    private fun <T> eventSubjectCheck(eventClass: Class<T>) {
        if (!mEventBusSubjectMap.containsKey(eventClass))
            mEventBusSubjectMap[eventClass] = PublishSubject.create<Any>().toSerialized()
    }

    private fun <T> stateSubjectCheck(stateClass: Class<T>) {
        if (!mStateBusSubjectMap.containsKey(stateClass))
            mStateBusSubjectMap[stateClass] = BehaviorSubject.create<Any>().toSerialized()
    }

    private fun getScheduler(@DeliveryThread deliveryThread: Int): Scheduler {
        return when (deliveryThread) {
            DeliveryThread.MAIN_THREAD -> AndroidSchedulers.mainThread()
            DeliveryThread.IO_THREAD -> Schedulers.io()
            DeliveryThread.NEW_THREAD -> Schedulers.newThread()
            else -> AndroidSchedulers.mainThread()
        }
    }

}
