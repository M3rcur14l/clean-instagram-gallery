package com.clean.instagramgallery.data.datasource

import com.clean.instagramgallery.data.mapper.asDomainModel
import com.clean.instagramgallery.data.service.InstagramService
import com.clean.instagramgallery.domain.model.Feed
import io.reactivex.Single
import javax.inject.Inject

class InstagramRemoteDataSource @Inject constructor(
    private val instagramService: InstagramService
) : InstagramDataSource {

    fun getFeeds(accessToken: String): Single<List<Feed>> {
        return instagramService
            .getRecentMedia(accessToken)
            .map { media -> media.data.map { it.asDomainModel() } }
    }
}