package com.clean.instagramgallery.data.model.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "feed")
class RoomFeed(
    @PrimaryKey
    val id: String,
    val date: Long,
    val thumbnail: String,
    val image: String,
    val aspectRatio: Double,
    val caption: String,
    val likes: Int,
    val comments: Int
)