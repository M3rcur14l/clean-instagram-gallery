package com.clean.instagramgallery.data.manager

import android.content.Context
import android.content.Context.MODE_PRIVATE
import androidx.core.content.edit
import javax.inject.Inject

class InstagramAccountManager @Inject constructor(context: Context) {
    companion object {
        const val ACCOUNT_MANAGER_PREFERENCES = "ACCOUNT_MANAGER_PREFERENCES"
        const val ACCESS_TOKEN_KEY = "ACCESS_TOKEN_KEY"
    }

    private val preferences = context.getSharedPreferences(ACCOUNT_MANAGER_PREFERENCES, MODE_PRIVATE)
    var accessToken: String?
        get() = preferences.getString(ACCESS_TOKEN_KEY, null)
        set(value) = preferences.edit { putString(ACCESS_TOKEN_KEY, value) }

}