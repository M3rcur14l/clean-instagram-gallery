package com.clean.instagramgallery.data.model.remote

data class InstagramRecentMedia(
    val data: List<InstagramMediaData>
)

data class InstagramMediaData(
    val id: String,
    val user: InstagramUserData?,
    val images: InstagramImageData?,
    val created_time: String?,
    val caption: InstagramCaptionData?,
    val likes: InstagramInteractionData?,
    val comments: InstagramInteractionData?
)

data class InstagramUserData(
    val id: String,
    val full_name: String?,
    val profile_picture: String?,
    val username: String?
)

data class InstagramImageData(
    val thumbnail: InstagramMediaImage?,
    val low_resolution: InstagramMediaImage?,
    val standard_resolution: InstagramMediaImage?
)

data class InstagramMediaImage(
    val width: Int?,
    val height: Int?,
    val url: String?
)

data class InstagramCaptionData(
    val text: String?
)

data class InstagramInteractionData(
    val count: Int?
)