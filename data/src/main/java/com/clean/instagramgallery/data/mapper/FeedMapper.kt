package com.clean.instagramgallery.data.mapper

import com.clean.instagramgallery.data.model.local.RoomFeed
import com.clean.instagramgallery.data.model.remote.InstagramMediaData
import com.clean.instagramgallery.domain.model.Feed
import java.util.*

fun InstagramMediaData.asDomainModel() = Feed(
    id = id,
    date = Date(created_time?.toLong() ?: 0),
    thumbnail = images?.thumbnail?.url ?: "",
    image = images?.standard_resolution?.url ?: "",
    aspectRatio = images?.standard_resolution?.let { it.width?.toDouble()?.div(it.height?.toDouble() ?: 1.0) }
        ?: 1.0,
    caption = caption?.text ?: "",
    likes = likes?.count ?: 0,
    comments = comments?.count ?: 0
)

fun RoomFeed.asDomainModel() = Feed(
    id = id,
    date = Date(date),
    thumbnail = thumbnail,
    image = image,
    aspectRatio = aspectRatio,
    caption = caption,
    likes = likes,
    comments = comments
)

fun Feed.asLocalDataModel() = RoomFeed(
    id = id,
    date = date.time,
    thumbnail = thumbnail,
    image = image,
    aspectRatio = aspectRatio,
    caption = caption,
    likes = likes,
    comments = comments
)