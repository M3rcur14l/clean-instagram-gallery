package com.clean.instagramgallery.data.bus.state

import com.clean.instagramgallery.data.bus.annotation.ExecutionState

data class LoadingState(@ExecutionState val state: Int, val stringResource: Int? = null)
