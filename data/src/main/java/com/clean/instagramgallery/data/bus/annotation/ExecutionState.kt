package com.clean.instagramgallery.data.bus.annotation

import androidx.annotation.IntDef

@IntDef(ExecutionState.RUNNING_INITIAL, ExecutionState.RUNNING, ExecutionState.COMPLETED, ExecutionState.FAILED)
@kotlin.annotation.Retention(AnnotationRetention.SOURCE)
annotation class ExecutionState {
    companion object {
        const val RUNNING_INITIAL = 1
        const val RUNNING = 2
        const val COMPLETED = 0
        const val FAILED = -1
    }
}