package com.clean.instagramgallery.data.bus.annotation

import androidx.annotation.IntDef

@IntDef(DeliveryThread.MAIN_THREAD, DeliveryThread.IO_THREAD, DeliveryThread.NEW_THREAD)
@Retention(AnnotationRetention.SOURCE)
annotation class DeliveryThread {
    companion object {
        const val MAIN_THREAD = 0
        const val IO_THREAD = 1
        const val NEW_THREAD = 2
    }
}