package com.clean.instagramgallery.domain.model

import java.util.*

class Feed(
    val id: String,
    val date: Date,
    val thumbnail: String,
    val image: String,
    val aspectRatio: Double,
    val caption: String,
    val likes: Int,
    val comments: Int
)