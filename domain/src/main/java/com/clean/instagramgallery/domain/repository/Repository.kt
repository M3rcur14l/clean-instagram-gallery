package com.clean.instagramgallery.domain.repository

import com.clean.instagramgallery.domain.LifecycleBinder
import com.clean.instagramgallery.domain.model.Feed
import io.reactivex.Flowable

interface Repository : LifecycleBinder {

    fun getFeed(): Flowable<List<Feed>>

}