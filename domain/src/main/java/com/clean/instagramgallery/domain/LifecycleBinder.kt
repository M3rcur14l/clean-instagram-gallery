package com.clean.instagramgallery.domain

import androidx.lifecycle.LifecycleOwner

interface LifecycleBinder : LifecycleOwner {

    fun bindTo(lifecycleOwner: LifecycleOwner)

}